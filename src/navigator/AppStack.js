import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';
import MainNavigator from './MainNav';
import Login from '../screens/LoginScreen';
import GroupPage from '../screens/GroupScreen'

const Stack = createStackNavigator();

const AppStack = (props) => {
	return (
		<NavigationContainer>
			<Stack.Navigator>
				{props.statusLogin ? (
					<>
						<Stack.Screen
							name="Main"
							component={MainNavigator}
							options={{headerShown: false}}
						/>
						<Stack.Screen
							name="Buat Grup"
							component={GroupPage}
							// options={{headerShown: false}}
						/>
					</>
				) : (
					<Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
};

const mapStateToProps = (state) => ({
	statusLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);
