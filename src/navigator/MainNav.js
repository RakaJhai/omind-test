import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';import {Text} from 'react-native';
import HomeScreen from '../screens/HomeScreen';
import LiveScreen from '../screens/LiveScreen';
import DiskusiScreen from '../screens/DiskusiScreen';
import ProfileScreen from '../screens/ProfilScreen';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
	return (
		<Tab.Navigator
			screenOptions={({route}) => ({
			tabBarIcon: ({focused, color, size}) => {
			  let iconName;
			  let iconProfile;
			  if (route.name === 'Home') {
				iconName = focused ? 'home-outline' : 'home-outline';
				return <Ionicons name={iconName} size={size} color={color} />;
			  } else if (route.name === 'Live') {
				iconName = focused ? 'videocam-outline' : 'videocam-outline';
				return <Ionicons name={iconName} size={size} color={color} />;
			  } 
			  else if (route.name === 'Diskusi') {
				iconName = focused ? 'chatbubbles-outline' : 'chatbubbles-outline';
				return <Ionicons name={iconName} size={size} color={color} />;
			  } 
			  else if (route.name === 'Profil') {
				iconName = focused ? 'person' : 'person';
				return <Ionicons name={iconName} size={size} color={color} />;
			  } 
			},
		  })}
		  tabBarOptions={{
			// activeTintColor: '#fff',
			inactiveTintColor: 'gray',
		  }}
		//   tabBarOptions={{sho/wLabel: false}}
		>
			<Tab.Screen name="Home" component={HomeScreen} />
			<Tab.Screen name="Live" component={LiveScreen} />
			<Tab.Screen name="Diskusi" component={DiskusiScreen} />
			<Tab.Screen name="Profil" component={ProfileScreen} />
		</Tab.Navigator>
	)

}

export default MainNavigator;