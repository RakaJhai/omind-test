import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';

function LoginPage(props){
	const [email, setEmail] = useState('tentor@gmail.com')
	const [password, setPassword] = useState('123123')


	const submit = () => {
      props.processLogin({email, password});
  };

	return (
		<View style={{justifyContent: 'center', flex: 1, marginHorizontal: 20}}>
			<Text style={{alignSelf:'center', marginVertical:20, fontSize: 20}}>Hello, Selamat Datang</Text>
			<TextInput
				value={email}
				onChangeText={(text) => setEmail(text)}
				placeholder='Enter Email'
				style={styles.textInput}
			/>
			<TextInput
				value={password}
				onChangeText={(text) => setPassword(text)}
				secureTextEntry
				placeholder='Enter Password'
				style={styles.textInput}
			/>

			<TouchableOpacity
				onPress={() => submit()}
				style={styles.button}
			>
				<Text >Login</Text>
			</TouchableOpacity>
		</View>
		)
}

const mapStateToProps = (state) => ({
	
});

const mapDispatchToProps = (dispatch) => ({
	processLogin: (data) => dispatch({type: 'LOGIN', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

const styles = StyleSheet.create({
	button:{
		alignItems: 'center',
		backgroundColor: "#008ae6",
		padding: 15,
		marginVertical: 20,
		borderRadius: 10
	},

	textInput:{
		borderBottomWidth: 0.5, 
		borderBottomColor: '#000'
	}
})