import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';

function GroupPage(props){
	const [name, setName] = useState('KELAS XII IPA 4 - SIANG. 1')
	const [jenjang, setJenjang] = useState()
	const [image, setImage] = useState()
	const [rawImage, setRawImage] = useState()

	
	const options = {
		title: 'Select Image',
		storageOptions: {
		skipBackup: true,
		path: 'images',
		},
	};
	function pickImage() {
	    ImagePicker.showImagePicker(options, (response) => {

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	      } else {
	        const source = {
	          uri: response.uri,
	          type: response.type,
	          name: response.fileName,
	          data: response.data,
	        };

	        setRawImage(source);
	        setImage(response.uri);
	        console.log(response.uri)
	      }
	    });
	  }

	  const submit = () => {
	  	const data = {
	  		kelas_id : jenjang,
	  		nama : name,
	  		thumbnail : image
	  	}
      props.addGroup(data);
  };

	return (
		<View style={{marginHorizontal: 20}}>
			{image == null ? (
				<TouchableOpacity
						onPress={() => pickImage() }
					>
					<View style={styles.iconCam}>
						<Ionicons name={'camera'} size={25} color={'#222'}/>
					</View>
					<Text style={styles.addImage}>Tambahkan Foto Group</Text>
				</TouchableOpacity>
				) :(
				<>
					<Image source={{uri : image}} style={styles.imagePict}/>
				</>
				)}
			
			
			<TextInput
				value={name}
				placeholder='Group Name'
				onChangeText={(text) => setName(text)}
				style={styles.textInput}
			/>
			<DropDownPicker
				items={[
					{label: '1', value: '1', hidden: true},
					{label: '2', value: '2'},
					{label: '3', value: '3'},
				]}
				
				containerStyle={{height: 40, width:'100%', marginVertical:5}}
				style={{borderRadius: 10, width: '100%'}}
				itemStyle={{
					justifyContent: 'flex-start'
				}}
				labelStyle={{
					color : '#222'
					}}
				placeholder='Jenjang'
				dropDownStyle={{backgroundColor: '#ddd'}}
				onChangeItem={item => setJenjang(item.value)}
			/>
			<View style={{alignItems: 'center'}}>
				<TouchableOpacity
					onPress={() => submit()}
					style={styles.button}
				>
					<Text style={{color: "#fff", fontWeight: '700', fontSize: 16}}>Buat Grup</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
	addGroup: (data) => dispatch({type: 'ADD_GROUP', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(GroupPage);

const styles = StyleSheet.create({
	textInput:{
		// backgroundColor:'#ddd',
		color:'#000',
		// borderRadius: 10,
		borderBottomWidth: 0.5,
		borderBottomColor: '#000'
	},
	button:{
		alignItems: 'center',
		backgroundColor: "#008ae6",
		padding: 15,
		width: '50%',
		borderRadius: 10
	},
	iconCam:{
		alignItems:'center', 
		marginTop: 20,
		width: 100, 
		height: 100,
		justifyContent:'center', 
		alignSelf:'center',
		borderRadius:10,
		borderWidth: 0.5,
		borderColor: '#000'
	},
	addImage:{
		alignSelf:'center', 
		marginVertical:20, 
		fontSize: 15, 
		color: '#777'
	},
	imagePict:{
		width:100,
		height:100,
		alignSelf:'center', 
		marginVertical:50
	}

})