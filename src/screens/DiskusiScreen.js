import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';

function DiskusiPage(props) {
	
	return (
		<>
			<View style={styles.header}>
				<Text style={{alignSelf:'center', color: '#fff', marginVertical:20, fontSize: 20}}>Diskusi</Text>
			</View>
			<View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
				<View style={styles.position}>
					{props.dataGroup == null ? (
						<Text style={styles.groupText}>Belum Ada Grup</Text>
					) : (
					<>
						
						<Text style={styles.groupText}>{props.dataGroup.nama}</Text>
						<Text style={styles.groupText}>Anggota Group : {props.dataGroup.anggota_grup}</Text>

					</>
					)}
					<TouchableOpacity
						onPress={() => props.navigation.navigate('Buat Grup')}
							style={styles.button}>
						<Text>Buat Grup</Text>
					</TouchableOpacity>
				</View>
			</View>
		</>
	);
}

const mapStateToProps = (state) => ({
	dataGroup: state.grup.data,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(DiskusiPage);

const styles = StyleSheet.create({
	groupText: {
		fontSize: 20,
		alignSelf: 'center',
		marginVertical: 10,
	},
	button:{
		alignItems: 'center',
		backgroundColor: "#008ae6",
		padding: 10,
		marginVertical:20,
		borderRadius: 10
	},
	header: {
		position:'absolute',
		backgroundColor: '#0099ff',
		width: "100%",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,

		elevation: 5,
	}
});
