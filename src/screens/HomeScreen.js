import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function HomePage(props){
	return (
		<View style={styles.header}>
			<Text style={{alignSelf:'center', color: '#fff', marginVertical:20, fontSize: 20}}>Home</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	header: {
		position:'absolute',
		backgroundColor: '#0099ff',
		width: "100%",
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,

		elevation: 5,
	}
})