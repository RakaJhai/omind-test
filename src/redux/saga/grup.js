import {takeLatest, put} from 'redux-saga/effects';
import {apiAddGroup} from './api/get_all_api';
import { getHeaders} from './api/async_storage';
import {ToastAndroid} from 'react-native';

function* addGrup(action) {
  try {
    const headers = yield getHeaders();

    const resGroup = yield apiAddGroup(action.payload, headers);
    

    yield put({type: 'ADD_GROUP_SUCCESS', payload: resGroup.data});

    ToastAndroid.showWithGravity(
      'Add group succesfully',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  } 
  catch(err) {
  	console.log('Error add group', JSON.stringify(err))
    yield put({type: 'ADD_GROUP_FAILED'});

    ToastAndroid.showWithGravity(
      'Add group unsuccesfully',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  }
}

function* grupSaga() {
  yield takeLatest('ADD_GROUP', addGrup);
}

export default grupSaga;
