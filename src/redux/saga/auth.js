import {takeLatest, put} from 'redux-saga/effects';
import {apiLogin} from './api/get_all_api';
import {
  removeToken,
  removeAccountId,
  saveAccountId,
  saveToken,
} from './api/async_storage';
import {ToastAndroid} from 'react-native';

function* login(action) {
  try {
    const resLogin = yield apiLogin(action.payload);

    if (resLogin && resLogin.data) {
      yield saveToken(resLogin.data.data.token_access);

      yield put({type: 'LOGIN_SUCCESS'});
      
      ToastAndroid.showWithGravity(
        'Login successfully',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    } else {
      ToastAndroid.showWithGravity(
        'Login Unsuccessfully',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  } 
  catch(err) {
    console.log("Error login", JSON.stringify(err))
    yield put({type: 'LOGIN_FAILED'});

    ToastAndroid.showWithGravity(
      'Gagal Login',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    )
  }
}

function* authSaga() {
  yield takeLatest('LOGIN', login);
}

export default authSaga;
