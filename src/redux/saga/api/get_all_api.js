import axios from 'axios';

// Api for login
export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: 'http://bta70.omindtech.id/api/tentor/login',
    data: dataLogin,
  });
}

// Api to Add grup
export function apiAddGroup(dataGroup, headers) {
  return axios({
    method: 'POST',
    url: 'http://bta70.omindtech.id/api/grup',
    data: dataGroup,
    headers
  });
}