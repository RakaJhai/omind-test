import {combineReducers} from 'redux';
import auth from './auth';
import grup from './grup';

export default combineReducers({
  auth,
  grup
});
