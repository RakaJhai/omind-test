const initialState = {}

const grup = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_GROUP': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'ADD_GROUP_SUCCESS': {
      console.log('reducer success', action.payload)
      return {
        ...state,
        isLoading: false,
        ...action.payload
      };
    }
    case 'ADD_GROUP_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default grup;