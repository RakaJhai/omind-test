import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store';
import AppStack from './src/navigator/AppStack';

const App = () => {
  return (
    <Provider store={store}>
      <AppStack />
    </Provider>
  );
};


export default App;
